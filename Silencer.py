import sys
import time
from config import initializer

initializer.installer()

from src import devil_bnr
from colorama import Fore
from utils import Build, Ransom
from config import help

while True:
    devil_bnr()
    time.sleep(1.1)
    print(
        Fore.LIGHTYELLOW_EX
        + "\n\t***  created  by cyrus ── instagram :_awmirsn_  ***\n\n"
        + Fore.LIGHTCYAN_EX
        + "[1]   -Create Ransomware-\n"
        + "[2]   -Build python .exe file-\n\n"
        + Fore.LIGHTWHITE_EX
        + "[01]   -help-\n"
        + "[00]   -Exit-\n\n"
    )
    try:
        chose = input(
            Fore.LIGHTCYAN_EX
            + "┌──["
            + Fore.LIGHTBLUE_EX
            + " Silencer "
            + Fore.LIGHTCYAN_EX
            + "] ──"
            + Fore.LIGHTWHITE_EX
            + R" \\ "
            + Fore.YELLOW
            + " Home"
            + Fore.LIGHTWHITE_EX
            + R" \\  "
            + Fore.LIGHTCYAN_EX
            + "\n└───$"
            + Fore.LIGHTWHITE_EX
            + " input : "
        )
    except KeyboardInterrupt:
        sys.exit()

    if chose == "1":
        Ransom().main()
    elif chose == "2":
        Build().main()
    elif chose == "3":
        devil_bnr()
    elif chose == "01":
        help.helper()
    elif chose == "00":
        break
    else:
        print(Fore.RED + "Value error! you have to enter a number between 1-4,Try again!...\n\n")
