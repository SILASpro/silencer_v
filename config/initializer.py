import os
from subprocess import check_output, Popen
import time
import sys
from getpass import getuser

pip_list = check_output("pip list", shell=True).decode().split()
pipNot_exist = []
user = getuser()


def pip_checker():
    if "tqdm" not in pip_list:
        os.system("pip install tqdm")
        os.system("cls")

    if "colorama" not in pip_list:
        pipNot_exist.append("colorama")

    if "requests" not in pip_list:
        pipNot_exist.append("requests")

    if "cryptography" not in pip_list:
        pipNot_exist.append("cryptography")

    if "win10toast" not in pip_list:
        pipNot_exist.append("win10toast")

    if "Pillow" not in pip_list:
        pipNot_exist.append("Pillow")

    if "pyinstaller" not in pip_list:
        pipNot_exist.append("pyinstaller")


print(pipNot_exist)


def installer():
    pip_checker()
    from tqdm import tqdm
    if len(pipNot_exist) == 0:
        pass
    else:
        print("\nInitializing ...\ndont close the App!..\n\n")
        for item in tqdm(pipNot_exist):
            time.sleep(2)
            with open(fR"C:\Users\{user}\SLlog.txt", "w") as File:
                try:
                    os.system(f"pip install {item}")
                except:
                    print("please check your internet connection and try again!..\n")
                    sys.exit()
